# Editor

## VIm/Neovim

You can use plugin that I(EndeyshetLabs) created

### vim.plug

```vim
Plug 'EndeyshetLabs/vim-porth-syntax'
```

### Packer

```lua
use 'EndeyshetLabs/vim-porth-syntax'
```

## Other Editors

Just use files from this directory
