# 3PM (Porth Plus Package Manager)

3PM is the package manager for porth-plus.

## Usage

### Listing packages

```console
$ 3pm list
```

### Installing a package

```console
$ 3pm install EndeyshentLabs/porth-plus-example-package
```

### Updating package

```console
$ 3pm update porth-plus-example-package
```

## How it works?

It is basically a wrapper around `git`. Currently supports only github.

## Dependencies

- git
- awk
- coreutils
- porth
