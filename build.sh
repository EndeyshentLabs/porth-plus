#!/usr/bin/env bash

set -xe

PREFIX=/usr/local/

function bootstrap {
    fasm -m 524288 ./bootstrap/porth-linux-x86_64.fasm
    chmod +x ./bootstrap/porth-linux-x86_64
}

function compiler {
    bootstrap
    ./bootstrap/porth-linux-x86_64 com ./porth.porth
}

function selfhost {
    compiler
    ./porth com ./porth.porth
}

function install {
    selfhost
    mkdir -p "$PREFIX/lib/porth"
    cp -r ./std/ "$PREFIX/lib/porth"
    if [[ -f "$PREFIX/bin/porth" ]]; then
        rm "$PREFIX/bin/porth"
    fi
    if [[ -f "$PREFIX/bin/3pm" ]]; then
        rm "$PREFIX/bin/3pm"
    fi
    ln -s "$PWD/porth" "$PREFIX/bin/porth"
    ln -s "$PWD/3pm/3pm" "$PREFIX/bin/3pm"
}

if [[ "$1" == "install" ]]; then
    install
else
    selfhost
fi

# vim:set ft=zsh:
